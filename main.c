#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define MAX_MATRI 8+1
#define MAX_MARCA 20+1
#define MAX_MODEL 30+1
#define MAX 5
#define NOM_FITXER "cotxes.dat"
#define NOM_FITXER2 "informe.html"
#define bb while(getchar()!='\n');

typedef struct {
    char matricula[MAX_MATRI];
    char marca[MAX_MARCA];
    char model[MAX_MODEL];
    int numeroPortes;
    int potencia;
    double consum; //%lf
    bool teEtiquetaECO;
} Cotxe;

void borrarPantalla();
void mostrarMenu();
int opcioMenuTriada();
int alta(char nomfitxer[]);
void entrarCotxe(Cotxe *c);
int llistat(char nomFitxer[]);
void escriureCotxe(Cotxe c);
int generarHTML(Cotxe c, char nomFitxer2[], char nomFitxer[], char estructuraHTML1[], char estructuraHTML2[]);
/*
Crear una estructura per salvar dades de cotxes.

Matricula       (cadena)
Marca           (cadena)
Model           (cadena)
NumeroPortes    (enter)
Potencia        (enter)
Consum          (double)
TeEtiquetaECO   (booleà)

***MENÚ***
1) Alta
2) Llistat

0) Sortir

***********
1) a) Demana a l'usuari, per teclat, les dades d'un cotxe.
   b) Un cop introduides cal persistir-les al fitxer ("cotxes.dat")
   c) Un cop feta la persistència de les dades cal demanar a l'usuari
      si vol introduir les dades d'un altre cotxe. Cas afirmatiu
      retorna al punt (a). Altrament retorna al menú.
2) Llistar per pantalla totes les dades del fitxer i fer una pausa.
   En prémer qualsevol tecla retorna al menú.
0) Acaba el programa.

system("clear");
*/

int main()
{
    int error,o;
    char estructuraHTML1[]=" <!DOCTYPE html>\n\
                            <html lang=\"es\">\n\
                            <head>\n\
                            \t<meta charset=\"UTF-8\">\n\
                            \t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\
                            \t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\
                            \t<title>Document</title>\n\
                            \t<link rel=\"stylesheet\" href=\"styles.css\">\n\
                            \t<link rel=\"stylesheet\" href=\"reset.css\">\n\
                            </head>\n\
                            <body class=\"app\">\n\
                            \t<header class=\"header\">\n\
                            \t\t<h1 class=\"title\">INFORME DE COTXES</h1>\n\
                            \t</header>\n\
                            \t\t<!-- table.table>tr*4>td*7{$} -->\n\
                            \t\t<!-- tr>th*7 -->\n\
                            \t<article class=\"article\">\n\
                            \t\t<table class=\"table\">\n\
                            \t\t\t<tr>\n\
                            \t\t\t\t<th>Matricula</th>\n\
                            \t\t\t\t<th>Marca</th>\n\
                            \t\t\t\t<th>Model</th>\n\
                            \t\t\t\t<th>Num. portes</th>\n\
                            \t\t\t\t<th>Potencia</th>\n\
                            \t\t\t\t<th>Consum</th>\n\
                            \t\t\t\t<th>Etiqueta ECO</th>\n\
                            \t\t\t</tr>\n";
    char estructuraHTML2[]=" \t\t</table>\n\
                            \t</article>\n\
                            \t<footer class=\"footer\">\n\
                            \t\tInstitut Almatà\n\
                            \t</footer>\n\
                            </body>\n\
                            </html>\n";


    do{
        borrarPantalla();
        mostrarMenu();
        o=opcioMenuTriada();
        switch(o){
        case 1:
            error=alta(NOM_FITXER);
            if(error==-1) printf("Error en obrir el fitxer");
            if(error==-2) printf("Error d'escriptura");
            break;
        case 2:
            error=llistat(NOM_FITXER);
            if(error==-1) printf("Error en obrir el fitxer");
            if(error==-3) printf("Error de lectura");
            break;
        case 0:
            //res
            break;
        default:
            printf("Les opcions valides són de 0 a 2");
        }
    }while(o!=0);

    int generarHTML(Cotxe c,nomFitxer2[],nomFitxer[],estructuraHTML1[],estructuraHTML2[]);



return 0;
}

//////////////////////////////////////////////////////////////////
////////////////////////ACCIONS I FUNCIONS////////////////////////
//////////////////////////////////////////////////////////////////

void borrarPantalla(){
    system("clear");
}

void mostrarMenu(){
    printf("* * * * * MENU * * * * *\n");
    printf("\t1. Alta \n");
    printf("\t2. Llistat \n");
    printf("\t0. Sortit \n");
    printf("* * * * * * * * * * * *\n");
}

int opcioMenuTriada(){
    int o;
    o=0;
    printf("Elegeix una de les opcions (1,2,0): ");
    scanf("%d",&o);bb;
    return o;
}

int alta(char nomfitxer[]){
    Cotxe c1;
    FILE *f1;
    int n;
    char sn;
    f1= fopen(nomfitxer,"ab");
    if( f1 == NULL ) {
        printf("Error en obrir el fitxer ");
        return -1;
    }

    do{
        entrarCotxe(&c1);
        n= fwrite(&c1,sizeof(Cotxe),1,f1);
        if(n==0){
            printf("error d'escriptura");
            return -2;
        }
        do{
            printf("\nVols introduir un altre cotxe? (s/n) ");
            scanf("%c",&sn);bb;
        }while(sn!='s' && sn!='n');
    }while(sn=='s');

    fclose(f1);
    return 0;
}

void entrarCotxe(Cotxe *c){
    char etiquetaECO;
    printf("\n\t-Introdueix la matricula: ");
    scanf("%7[^\n]",c->matricula);bb;
    printf("\n\t-Introdueix la marca: ");
    scanf("%20[^\n]",c->marca);bb;
    printf("\n\t-Introdueix la model: ");
    scanf("%30[^\n]",c->model);bb;
    printf("\n\t-Introdueix el numero de portes: ");
    scanf("%i",&c->numeroPortes);bb;
    printf("\n\t-Introdueix la potencia: ");
    scanf("%i",&c->potencia);bb;
    printf("\n\t-Introdueix el consum: ");
    scanf("%lf",&c->consum);bb;
    do{
        printf("\n\t-Té etiqueta ECO? (s/n): ");
        scanf("%c",&etiquetaECO);bb;
    }while(etiquetaECO!='s' && etiquetaECO!='n');

    if(etiquetaECO=='s') c->teEtiquetaECO=true;
    else c->teEtiquetaECO=false;
}

int llistat(char nomFitxer[]){
    FILE *f2;
    Cotxe c2;
    int n;

    f2= fopen(nomFitxer,"rb");
    if( f2 == NULL ){
        printf("Error en obrir el fitxer ");
    return -1;
    }


    while(!feof(f2)){
        n = fread(&c2,sizeof(Cotxe), 1,f2);
        if(!feof(f2)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }
            escriureCotxe(c2);
        }
    }
    fclose(f2);
    printf("\nPrem una tecla per continuar...");getchar();
    //scanf("%c",&continuar);bb;
    return 0;
}

void escriureCotxe(Cotxe c){
    printf("\n\t-Matricula: %s\n", c.matricula);
    printf("\t-Marca: %s\n",c.marca);
    printf("\t-Model: %s\n",c.model);
    printf("\t-Numero de portes: %i\n",c.numeroPortes);
    printf("\t-Potencia: %i\n",c.potencia);
    printf("\t-Consum: %lf\n",c.consum);
    if(c.teEtiquetaECO) printf("\t-Te etiqueta ECO\n");
    else printf("\t-No te etiqueta ECO\n");

}

int generarHTML(Cotxe c, char nomFitxer2[], char nomFitxer[], char estructuraHTML1[], char estructuraHTML2[]){
    //////
    FILE *f1,*f2;
    //////

    //Cotxe c2;
    //int n;


    //////obrir fitxer html
    f1= fopen(nomFitxer2,"w");
    if( f1 == NULL ){
        printf("Error en obrir el fitxer ");
        return -1;
    }

    //////obrir fitxer dat
    f2= fopen(nomFitxer,"r");
    if( f2 == NULL ){
        printf("Error en obrir el fitxer ");
        return -1;
    }

    int fputs(const char *estructuraHTML1, FILE *f1);




    //////

    while(!feof(f2)){
        n = fread(&c2,sizeof(Cotxe), 1,f2);
        if(!feof(f2)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }

            strcat(total,"\t\t\t<tr>");
            strcat(total,"\t\t\t\t<td>");
            strcat(total,c.matricula);
            strcat(total,"</td>");
            strcat(total,"\t\t\t\t<td>");
            strcat(total,c.marca);
            strcat(total,"</td>");
            strcat(total,"\t\t\t\t<td>");
            strcat(total,c.model);
            strcat(total,"</td>");
            strcat(total,"\t\t\t\t<td>");
            strcat(total,c.numeroPortes);
            strcat(total,"</td>");
            strcat(total,"\t\t\t\t<td>");
            strcat(total,c.potencia);
            strcat(total,"</td>");
            strcat(total,"\t\t\t\t<td>");
            strcat(total,c.consum);
            strcat(total,"</td>");
            strcat(total,"\t\t\t\t<td>");
            strcat(total,c.teEtiquetaECO);
            strcat(total,"</td>");
            strcat(total,"\t\t\t</tr>");

        }
    }

    //////
    int fputs(const char *estructuraHTML2, FILE *f1);
    fclose(f1);

    fclose(f2);
    //////

    return 0;
}
